---
title: Czcionka dyslektyczna
slug: dyslektyczna
---

# Czcionka dyslektyczna

Obecna technologia (2023 rok) pozwala bez problemu na dostosowanie stylu treści by ta była czytelna dla, w miare możliwości, dużej części osób.

## Dysleksja

Jest to zaburzenie między innymi percepcji wzrokowej – objawiającej się widzeniem, ale niedostrzeganiem. Jednym z objawów jest mylenie liter podczas czytania lub pisania. <sup>1</sup>

Z pomocą mogą przyjść czcionki dedykowane osobom, którym trudność sprawia czytanie lub pisanie tekstu wyświetlanego czcionkami wykorzystywanymi standardowo w systemach komputerowych (między innymi w notatnikach lub przeglądarkach internetowych - strony), a tymi standardowymi są na przykład "Times New Roman" lub "Arial".

## Czcionka Hiperczytelna Atkinsona ("Atkinson Hyperlegible")

To w pełni darmowa czcionka, której każdy znak jest unikalnie zapisany. W szczególności dotyczy to znaków wyglądających podobnie, jak na przykład: duże "o" i zero, "q" i "p", cyfra jeden i duże "i", "E" i "F", czy cyfry sześć i dziewięć.<sup>2</sup>

Obraz: atkinson. <sup>3</sup>

## Czcionka dyslektyczna ("OpenDyslexic") {.font-dyslexic}

To kolejna, darmowa, czcionka poświęcona osobom z dysleksją. Stworzona i rozwijana jest oddolnie przez grupę osób. Czcionkę wyróżniają pogrubione dolne części liter, co nadaje im unikalność w całym alfabecie. <sup>4</sup>

Obraz: dyslexic-font-compare <sup>5</sup>

## Porównanie wymienionych czcionek z popularnymi

<p class="font-sans">
Sans: 0123456789 qwertyuiopasdfghjklzxcvbnm ęóąśłżźćń
</p>
<p class="font-serif">
Szeryfowa: 0123456789 qwertyuiopasdfghjklzxcvbnm ęóąśłżźćń
</p>
<p class="font-mono">
Mono: 0123456789 qwertyuiopasdfghjklzxcvbnm ęóąśłżźćń
</p>
<p class="font-atkinson">
Atkinson: 0123456789 qwertyuiopasdfghjklzxcvbnm ęóąśłżźćń
</p>
<p class="font-dyslexic">
Dyslektyczna: 0123456789 qwertyuiopasdfghjklzxcvbnm ęóąśłżźćń
</p>

---

Inne polecone czcionki ułatwiające czytanie to:
- Fira Mono: [https://en.wikipedia.org/wiki/Fira_(typeface)](https://en.wikipedia.org/wiki/Fira_(typeface))


Źródła:

1: Dyslekcja: [https://pl.wikipedia.org/wiki/Dysleksja](https://pl.wikipedia.org/wiki/Dysleksja)

2,3: Czionka Hiperczytelna Atkinsona: [https://brailleinstitute.org/freefont](https://brailleinstitute.org/freefont)

4,5: Czcionka Dyslektyczna: [https://opendyslexic.org/](https://opendyslexic.org/)


