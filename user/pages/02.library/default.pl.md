---
title: Biblioteka
slug: biblioteka
---

Biblioteka wpisów dotyczących porad cyfrowych, opisów narzędzi i platform, a także wskazówek w ich użytkowaniu.

## Interfejst użytkowniczy

- Czcionka dyslektyczna: [link](/biblioteka/dyslektyczna)
- Inkluzywność graficzna: w trakcie

## Wolna komunikacja internetowa

- Signal, Molly: w trakcie

## Wolne sklepy z aplikacjami Android

- F-Droid: w trakcie

## Otwartoźródłowe i wolne dyski / przestrzenie chmurowe online

- Nextcloud: w przyszłości
- Humhub: w przyszłości

## DNS

- Europejski serwer DNS: w trakcie

## Narzędzia IT dostępne online

- Systemy operacyjne w sandboxie: w trakcie
- Środowiska programistyczne online: w trakcie