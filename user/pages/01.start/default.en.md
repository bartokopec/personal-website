---
title: Start
body_classes: 'title-center title-h1h2'
---

# Foxay - digital activism

Here you can find [Library](/library), with posts about digital tips, descriptions of tools and platforms, and guidance in their use.

Also, I work activistically at [Association of Mutually Support](https://wspolneoparcie.org) and culturally at [Grassroot Socio-Cultural Centre Standstill](https://ocsk-postoj.wspolneoparcie.org)