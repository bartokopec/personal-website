---
title: Start
body_classes: 'title-center title-h1h2'
---

# Foxay - cyfrowy aktywizm

Można tutaj znaleźć [Bibliotekę](/biblioteka), zawierającą wpisy o poradach cyfrowych, opisach narzędzi i platform, a także wskazówkach w ich użytkowaniu, a także [listę projektów](/projekty) jakie współtworzę

Udzielam także wsparcia informatycznego, korepetycji, czy pomocy w programowaniu: [Korepetycje](/korepetycje)

Poza tym, aktywistycznie pracuję w [Stowarzyszeniu Wspólne Oparcie](https://wspolneoparcie.org) oraz kulturowo w [Oddolnym Centrum Społeczno Kulturalnym Postój](https://ocsk-postoj.wspolneoparcie.org)