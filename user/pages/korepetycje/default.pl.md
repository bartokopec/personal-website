---
title: Korepetycje Informatyczne
slug: korepetycje
---

# Korepetycje Informatyczne / Pomoc informatyczna ogólnie

Korepetycje Informatyka / Pomoc w programowaniu / Pomoc informatyczna ogólnie

Stawka za godzinę: 60 pln

Hejo, jestem Barto (ono/jeno). Ukończyłom technikum oraz studia informatyczne, w większości z obszaru programowania. Udzielam korepetycji na poziomie szkoły podstawowej, średniej i wyższej, jak i wsparcia informatycznego z:
- nauki programowania
- wsparcia komputerowego
- zadań dotyczących pakietu biurowego (różne Office)
- obsługi baz danych

Programowaniem i administracją bazami danych zajmuję się od kilkunastu lat. Poznałom wiele języków i technologii z tym związanych, a w niektórych z nich aktywnie zawodowo pracuję: SQL, noSQL, C#, Python, PHP, Javascript (a w tym React, Angular, Svelte), CSS, Kotlin, Java, Android, Ionic, i inne. Aktywnie działam w obszarze Open Source i używaniem niekorporacyjnych projektów programistycznych.
Odkryłom w sobie także dużą chęć przekazywania zdobytej przeze mnie wiedzy z tematyki opisanej powyżej. Stąd między innymi pomysł na udzielanie korepetycji czy warsztatów.

Jestem też osobą nie kierującą się sztywnymi regułami i działaniem ponad siły.
Umawiając się na konkretny termin, bez problemu można taki przełożyć - zawsze coś może wypaść po drodze i to nic złego.
Nie biorę zaliczek. Płatność rozliczana jest dopiero po godzinie spotkania.
Lepiej czuję się w równej relacji mentorskiej, ponad hierarchię autorytetu.
Tematyka spotkania, czy to faktyczne korepetycje, czy wsparcie w przygotowaniu do egzaminu lub zadaniu domowym, może luźno ustalona indywidualnie.

Kontaktować się można ze mną dzwoniąc lub wysyłając sms’a na numer 692 007 782 lub pisząc na ten numer na Signalu. Nie używam Whatsapp’a.
Dostępne jestem w większość czasu. Najłatwiej najpierw wysłać mi wiadomość tekstową.