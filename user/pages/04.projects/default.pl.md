---
title: Projekty
slug: projekty
---

Wydarzenia współorganizowane:

- [LAN Party](/projekty/lan-party)

Lista projektów programistycznych, które współtworzę:

- Strona Foxay
- Strony grup: Wspólne Oparcie, OCSK Postój

Lista oddolnych inicjatyw, które współtworzę:

- Oddolne Centrum Społeczno Kulturalne Postój