---
title: 'LAN Party'
media_order: lan-party-baner.jpg
---

![lan party baner](lan-party-baner.jpg?resize=700)

Zapraszamy na Linuksowo Aktywną Niedzielę!

30 lipca, od godziny 16 rozpoczniemy off-line warsztat linuksowy.
- przybliżymy czym są system operacyjny i Linuks;
- podzielimy się własnym doświadczeniem i ciekawostkami z tematyki linuksowej;
- na koniec będzie czas i możliwość na:
  - wsparcie w instalacji Linuksa
  - indywidualne porady
  - umówienie się na pomoc wzajemną dotyczącą osobistego linuksa

Warsztat będzie w Oddolnym Centrum Społeczno Kulturalnym "Postój", Wrocław, Metalowców 59b

Możesz przyjść z własnym komputerem/laptopem/pendrive.

Wydarzenie jest bezpłatne.