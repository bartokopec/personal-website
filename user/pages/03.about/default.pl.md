---
title: O nas
slug: o-nas
---

## Kontakt

bartokopec@riseup.net / barto@wspolneoparcie.org

https://kolektiva.social/@barto

https://szmer.info/u/barto


# O nas

Foxay to mój osobisty projekt poświęcony cyfrowemu aktywizmowi. Czyli dążeniu do zmiany w podstawach obecnej cyfrowej przestrzeni. Ta jest zdominowana przez korporacje, które niehumanitarnie i nierówno ją traktują.

Ten projekt zakłada dzielenie się wiedzą z użytkowania cyfrowych narzędzi i platform, które rozwijane są przez społeczność - inaczej nazywa się to otwartoźródłowością/otwartym oprogramowaniem (open source) <sup>1</sup>

## O mnie

Jestem Barto Kopeć, używam zaimków ono/jeno <sup>2</sup>.

Rozwijam oprogramowania i poznaję wiedzę z zakresu open source od 2017 roku. Przedtem, moimi początkami były niestety technologie stworzone przez korporacje. Z prostego powodu - łatwość dla mnie, w tamtym czasie, w dostępie do dokumentacji i tutoriali. 

Z biegiem lat zauważyłom, że to zmieniło się. Open Source zdobywał więcej popularności to i platformy, strony internetowe, na których można było rozpocząć swoją przygodę, unowocześniły się.

Tak, więc to, w czym się specjalizuję, to:

- języki programowania obiektowe lub zakładające taką funkcjonalność, jak na przykład PHP, Javascript, C#, Java/Kotlin, Python,
- technologie bazodanowe relacyjne i nierelacyjne,
- technologie frontendowe (HTLM, CSS),
- formatowania tekstu (MD, XML, CSV, JSON, i inne),
- edukacja o cyberbezpieczeństwie i programowaniu

---

1: Otwartoźródłowość [https://pl.wikipedia.org/wiki/Otwarte_oprogramowanie](https://pl.wikipedia.org/wiki/Otwarte_oprogramowanie)

2: Zaimki ono/jeno [https://zaimki.pl/ono/jeno](https://zaimki.pl/ono/jeno)