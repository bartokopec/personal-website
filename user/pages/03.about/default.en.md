---
title: About
slug: about
---

## Contact

bartokopec@riseup.net / barto@wspolneoparcie.org

https://kolektiva.social/@barto

https://szmer.info/u/barto

# About Foxay

Foxay is my personal project, dedicated to digital activism. That is, striving for change in the floor of the current digital space. This one is dominated by corporations that treat it inhumanely and unequally.

This project involves sharing knowledge from the use of digital tools and platforms that are developed by the community - otherwise known as open source <sup>1</sup>.

## About me

I am Barto Kopeć, I use the pronouns they/them <sup>2</sup>.

I've been developing software and learning about open source since 2017. My beginnings were unfortunately technologies created by corporations. For a simple reason - simplicity for me, at that time, in accessing documentation and tutorials. 

Over the years, I have noticed that this has changed. Open Source was gaining more popularity then and the platforms, the websites where you could start your adventure, upgraded.

So, what I specialise in are:

- object-oriented programming languages or those that assume such functionality, such as PHP, Javascript, C#, Java/Kotlin, Python, for example,
- relational and non-relational database technologies,
- front-end technologies (HTLM, CSS),
- text formatting (MD, XML, CSV, JSON, etc.),
- education about cybersecurity and programming

---

1: Open source [https://en.wikipedia.org/wiki/Open-source_software](https://en.wikipedia.org/wiki/Open-source_software)

2: They/them pronouns [https://en.pronouns.page/they/them/themself](https://en.pronouns.page/they/them/themself)